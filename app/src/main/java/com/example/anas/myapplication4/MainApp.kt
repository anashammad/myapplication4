package com.example.anas.myapplication4

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.HttpMethod
import org.json.JSONArray
import org.json.JSONObject
import java.util.*


class MainApp : AppCompatActivity() {
    var params = Bundle()
    val accessToken = AccessToken.getCurrentAccessToken()!!
    var facebookFriendsCallback = GraphRequest.Callback { response ->
        var myfriendsjson : JSONObject? = response.jsonObject
        var friendsArry :JSONArray? = myfriendsjson!!.getJSONArray("data")
        SetFriend(friendsArry!!)
        /* handle the result */
    }
    var request = GraphRequest.newMeRequest(
            accessToken
    ) { `object`, response ->
        var myfriendsjson = response.jsonObject

        // Application code
    }!!
    var friendsrequest =  GraphRequest (
            accessToken,
            "/"+accessToken.userId+"/friends",
            null,
            HttpMethod.GET,
            facebookFriendsCallback
    )
    var nameArray  = ArrayList<String>()




   open override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_app)
        val messageLst = findViewById(R.id.messageLst) as ListView
        val navigation = findViewById(R.id.navigation) as BottomNavigationView

       var messageLstadapter = object : ArrayAdapter<String>(this,android.R.layout.two_line_list_item,nameArray) {
           override fun getView(position: Int, view: View?, parent: ViewGroup): View {
               var view = view
               if (view == null) {
                   view = layoutInflater.inflate(android.R.layout.two_line_list_item, parent, false)
               }
               var friendName = nameArray[position]
               (view!!.findViewById(android.R.id.text1) as TextView).text = friendName
               return view
           }
       }
        messageLst.adapter = messageLstadapter
       navigation.setOnNavigationItemSelectedListener(BottomNavigationView.OnNavigationItemSelectedListener { item ->
           when (item.itemId) {
               R.id.navigation_home -> {

                   return@OnNavigationItemSelectedListener true
               }
               R.id.navigation_dashboard -> {
                   GetFaceFriends()



                   return@OnNavigationItemSelectedListener true
               }
               R.id.navigation_notifications -> {
                   messageLstadapter.notifyDataSetChanged()
                   return@OnNavigationItemSelectedListener true
               }
           }
           false
       })
    }
fun GetFaceFriends (){
    params.putString("fields", "name, picture")
    request.parameters = params
    request.executeAsync()
    friendsrequest.executeAsync()
}
    fun SetFriend(FriendsArray : JSONArray){
        var len : Int = FriendsArray.length()
        for (i in 1..len){
            nameArray.add(FriendsArray.getJSONObject(i)!!.getString("name"))
            }
    }
}
