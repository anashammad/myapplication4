package com.example.anas.myapplication4

import android.app.LoaderManager.LoaderCallbacks
import android.content.Intent
import android.content.Loader
import android.database.Cursor
import android.os.Bundle
import android.provider.AlarmClock
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.accountkit.AccessToken
import com.facebook.accountkit.AccountKit
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.firebase.client.Firebase
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.*
/**
 * A login screen that offers login via email/password.
 */
open class LoginActivity : AppCompatActivity(), LoaderCallbacks<Cursor> {

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    var mCallbackManager: CallbackManager? = null
    var mEmailView: AutoCompleteTextView? = null
    var loginButton: LoginButton? = null
    var mPasswordView: EditText? = null
    var credential: AuthCredential? = null
    var email: String? = null
    var password: String? = null
    var ref: Firebase? = null
    var accessToken : AccessToken? = null
    var mAuth: FirebaseAuth? = null
    var mEmailSignInButton: Button? = null
    var user: FirebaseUser? = null
    var task: Task<AuthResult>? = null
    //var facebooktask: Task<AuthResult>? = null
    val Pack = "com.example.anas.myapplication4"
    val MainApp = "com.example.anas.myapplication4.MainApp"
    val mainAct = "com.example.anas.myapplication4.MainActivity"
    val facebookCallback = object : FacebookCallback<LoginResult> {
        override fun onSuccess(loginResult: LoginResult) {
            findViewById(R.id.login_Layout).visibility = View.INVISIBLE
            findViewById(R.id.spalsh).visibility = View.VISIBLE
            handleFacebookAccessToken(loginResult.accessToken)
        }

        override fun onCancel() {
        }

        override fun onError(error: FacebookException) {
            Toast.makeText(this@LoginActivity, "Authentication error.", Toast.LENGTH_LONG).show()
        }
    }
    val onFacebookTaskComplete = OnCompleteListener<AuthResult> { p0 ->
        if (p0.isSuccessful) {
            user = mAuth!!.currentUser
            Toast.makeText(this@LoginActivity, "Authentication sus.", Toast.LENGTH_LONG).show()
            gotoActivity(MainApp)
        } else {
            setContentView(R.layout.activity_login)
            Toast.makeText(this@LoginActivity, "Authentication failed.", Toast.LENGTH_SHORT).show()
        }
    }
    val onFirebaseTaskComplete = OnCompleteListener<AuthResult> { p0 ->
        if (p0.isSuccessful) {
            // Task completed successfully
            Toast.makeText(this@LoginActivity, "Authentication sus.", Toast.LENGTH_LONG).show()
            gotoActivity(mainAct)
        } else {
            // Task failed with an exception
            val exception = p0.exception
            val exceptionError = exception.toString().split(":")
            findViewById(R.id.login_Layout).visibility = View.VISIBLE
            findViewById(R.id.spalsh).visibility = View.INVISIBLE
            Toast.makeText(this@LoginActivity, "Authentication failed.", Toast.LENGTH_LONG).show()
            Toast.makeText(this@LoginActivity, exceptionError[1], Toast.LENGTH_LONG).show()
            task = null
        }
    }
    // ...
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        // Set up the login form.
        mEmailView = findViewById(R.id.email) as AutoCompleteTextView
        mPasswordView = findViewById(R.id.password) as EditText
        loginButton = findViewById(R.id.login_button) as LoginButton
        mEmailSignInButton = findViewById(R.id.email_sign_in_button) as Button
        mAuth = FirebaseAuth.getInstance()
       // mProgressView = findViewById(R.id.login_progress)
        loginButton!!.setReadPermissions("email", "public_profile","user_friends","read_custom_friendlists")
        mCallbackManager = CallbackManager.Factory.create()
        Firebase.setAndroidContext(this)
        ref = Firebase("https://myapplication4-f3e1e.firebaseio.com/")
        loginButton!!.registerCallback(mCallbackManager!!, facebookCallback)
        LoginManager.getInstance().registerCallback(mCallbackManager!!, facebookCallback)
        mEmailSignInButton!!.setOnClickListener {
            email = mEmailView!!.text.toString()
            password = mPasswordView!!.text.toString()
            if (!attemptLogin()) {
                findViewById(R.id.login_Layout).visibility = View.INVISIBLE
                findViewById(R.id.spalsh).visibility = View.VISIBLE
                mAuth = FirebaseAuth.getInstance()
                task = mAuth!!.signInWithEmailAndPassword(email!!, password!!)
                task!!.addOnCompleteListener(onFirebaseTaskComplete)
            }
        }

    }


    // Callback registration

    override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        accessToken = AccountKit.getCurrentAccessToken()
        if (accessToken != null) {
            gotoActivity(mainAct)
            //Handle Returning User
        } else {
            //Handle new or logged out user
        }
    }
    override fun onActivityResult( requestCode: Int,resultCode:Int, data:Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        mCallbackManager!!.onActivityResult(requestCode, resultCode, data)
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private fun attemptLogin() : Boolean{

        // Reset errors.
        mEmailView!!.error = null
        mPasswordView!!.error = null

        // Store values at the time of the login attempt.
        var cancel = false
        var focusView: View? = null

        // Check for a valid password, if the user entered one.
        if (password == null ) {
            mPasswordView!!.error = getString(R.string.error_invalid_password)
            focusView = mPasswordView
            cancel = true
        }   else if (!isPasswordValid(password!!)) {
            mPasswordView!!.error = getString(R.string.error_invalid_password)
            focusView = mPasswordView
            cancel = true
        }

        // Check for a valid email address.
        if (email == null) {
            mEmailView!!.error = getString(R.string.error_field_required)
            focusView = mEmailView
            cancel = true
        } else if (!isEmailValid(email!!)) {
            mEmailView!!.error = getString(R.string.error_invalid_email)
            focusView = mEmailView
            cancel = true
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView!!.requestFocus()

        }
        return cancel
    }

    fun isEmailValid(email: String): Boolean {

        return email.contains("@")
    }

    fun isPasswordValid(password: String): Boolean {

        return password.length > 4
    }


    fun gotoActivity(act: String) {
        val intent = Intent(act)
        intent.setClassName(Pack, act)
        intent.putExtra(AlarmClock.EXTRA_MESSAGE, "Logged in")
        startActivity(intent)

    }
    fun handleFacebookAccessToken(token: com.facebook.AccessToken) {
    credential = FacebookAuthProvider.getCredential(token.token)
    mAuth!!.signInWithCredential(credential!!).addOnCompleteListener(this, onFacebookTaskComplete)
    }
    override fun onLoadFinished(loader: Loader<Cursor>?, data: Cursor?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onLoaderReset(loader: Loader<Cursor>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

